class PrecedentsController < ApplicationController
  before_action :set_precedent, only: [:show, :edit, :update, :destroy]

  # GET /precedents
  # GET /precedents.json
  def index
    @precedents = Precedent.all.order(created_at: :desc).limit(9)
  end

  # GET /precedents/1
  # GET /precedents/1.json
  def show
  end

  # GET /precedents/new
  def new
    @precedent = Precedent.new
  end

  # GET /precedents/1/edit
  def edit
  end

  # POST /precedents
  # POST /precedents.json
  def create
    @precedent = Precedent.new(precedent_params)

    respond_to do |format|
      if @precedent.save
        format.html { redirect_to @precedent, notice: 'Precedent was successfully created.' }
        format.json { render :show, status: :created, location: @precedent }
      else
        format.html { render :new }
        format.json { render json: @precedent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /precedents/1
  # PATCH/PUT /precedents/1.json
  def update
    respond_to do |format|
      if @precedent.update(precedent_params)
        format.html { redirect_to @precedent, notice: 'Precedent was successfully updated.' }
        format.json { render :show, status: :ok, location: @precedent }
      else
        format.html { render :edit }
        format.json { render json: @precedent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /precedents/1
  # DELETE /precedents/1.json
  def destroy
    @precedent.destroy
    respond_to do |format|
      format.html { redirect_to precedents_url, notice: 'Precedent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_precedent
      @precedent = Precedent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def precedent_params
      params.require(:precedent)
            .permit(:title, :description,
                    sources_attributes: [:id, :url, :_destroy],
                    politicians_attributes: [:id, :name, :lastname, :_destroy])
    end
end
