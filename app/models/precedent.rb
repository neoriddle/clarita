class Precedent < ActiveRecord::Base
  has_and_belongs_to_many :politicians, auto_save: true
  has_and_belongs_to_many :sources, auto_save: true

  accepts_nested_attributes_for :politicians,
                                allow_destroy: true, 
                                reject_if: proc { |a| a[:name].blank? }
  accepts_nested_attributes_for :sources,
                                allow_destroy: true, 
                                reject_if: proc { |a| a[:url].blank? }

  validates :title, presence: true
end
