class PrecedentsSources < ActiveRecord::Base
  belongs_to :precedent
  belongs_to :source
end
