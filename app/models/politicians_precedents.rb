class PoliticiansPrecedents < ActiveRecord::Base
  belongs_to :politician
  belongs_to :precedent
end
