json.array!(@politicians) do |politician|
  json.extract! politician, :id, :name, :lastname
  json.url politician_url(politician, format: :json)
end
