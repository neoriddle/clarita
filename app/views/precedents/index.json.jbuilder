json.array!(@precedents) do |precedent|
  json.extract! precedent, :id, :title, :description
  json.url precedent_url(precedent, format: :json)
end
