class CreatePrecedents < ActiveRecord::Migration
  def change
    create_table :precedents do |t|
      t.string :title, limit: 50
      t.text :description
      t.datetime :time_stamp

      t.timestamps null: false
    end
  end
end
