class RemoveTimestampFromPrecedents < ActiveRecord::Migration
  def change
  	remove_column :precedents, :time_stamp, :datetime, {}
  end
end
