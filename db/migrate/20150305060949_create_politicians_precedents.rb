class CreatePoliticiansPrecedents < ActiveRecord::Migration
  def change
    create_table :politicians_precedents do |t|
      t.references :politician, index: true
      t.references :precedent, index: true

      t.timestamps null: false
    end
    add_foreign_key :politicians_precedents, :politicians
    add_foreign_key :politicians_precedents, :precedents
  end
end
