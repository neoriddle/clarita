class CreatePoliticians < ActiveRecord::Migration
  def change
    create_table :politicians do |t|
      t.string :name, limit: 30
      t.string :lastname, limit: 30

      t.timestamps null: false
    end
  end
end
