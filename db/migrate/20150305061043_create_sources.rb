class CreateSources < ActiveRecord::Migration
  def change
    create_table :sources do |t|
      t.string :url, limit: 50

      t.timestamps null: false
    end
  end
end
