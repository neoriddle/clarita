class CreatePrecedentsSources < ActiveRecord::Migration
  def change
    create_table :precedents_sources do |t|
      t.references :precedent, index: true
      t.references :source, index: true

      t.timestamps null: false
    end
    add_foreign_key :precedents_sources, :precedents
    add_foreign_key :precedents_sources, :sources
  end
end
