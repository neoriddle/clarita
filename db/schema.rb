# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150403073237) do

  create_table "politicians", force: :cascade do |t|
    t.string   "name",       limit: 30
    t.string   "lastname",   limit: 30
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "politicians_precedents", force: :cascade do |t|
    t.integer  "politician_id"
    t.integer  "precedent_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "politicians_precedents", ["politician_id"], name: "index_politicians_precedents_on_politician_id"
  add_index "politicians_precedents", ["precedent_id"], name: "index_politicians_precedents_on_precedent_id"

  create_table "precedents", force: :cascade do |t|
    t.string   "title",       limit: 50
    t.text     "description"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "precedents_sources", force: :cascade do |t|
    t.integer  "precedent_id"
    t.integer  "source_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "precedents_sources", ["precedent_id"], name: "index_precedents_sources_on_precedent_id"
  add_index "precedents_sources", ["source_id"], name: "index_precedents_sources_on_source_id"

  create_table "sources", force: :cascade do |t|
    t.string   "url",        limit: 50
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

end
