require 'test_helper'

class PrecedentsControllerTest < ActionController::TestCase
  setup do
    @precedent = precedents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:precedents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create precedent" do
    assert_difference('Precedent.count') do
      post :create, precedent: { description: @precedent.description, title: @precedent.title }
    end

    assert_redirected_to precedent_path(assigns(:precedent))
  end

  test "should show precedent" do
    get :show, id: @precedent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @precedent
    assert_response :success
  end

  test "should update precedent" do
    patch :update, id: @precedent, precedent: { description: @precedent.description, title: @precedent.title }
    assert_redirected_to precedent_path(assigns(:precedent))
  end

  test "should destroy precedent" do
    assert_difference('Precedent.count', -1) do
      delete :destroy, id: @precedent
    end

    assert_redirected_to precedents_path
  end
end
